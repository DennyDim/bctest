<?php

require_once __DIR__ . '/vendor/autoload.php';

use Bigcommerce\Api\Client as Bigcommerce;

//provided credentials 
$credentiasArr = array('client_id' =>  '123456', 'client_secret' => ' P$ssword123', 'redirect_uri' => 'https://app.com/redirect', 'store_hash' => '5h79jw');
cofigBCApi($credentiasArr);


// get all customers with email like @gmail.com
$filter = array( "email:like" => '@gmail.com');
$arrCustomers = Bigcommerce::getCustomers($filter);  

if(count($arrCustomers) > 0 ) {
	foreach ($arrCustomers as $customer) {
		echo $customer->id . ":" . $customer->name . " ". $customer->email . "\n";
	}
}



/**
 * Configure  BigCommerce API client with the authorized app's auth token, the client ID and tore's hash as provided from the credentials's array
 *@param string $credentiasArr array with provided credentials
 */
function cofigBCApi($credentiasArr) { 	
	Bigcommerce::configure(array(
	    'client_id' => $credentiasArr['client_id'],
	    'auth_token' =>getAuthToken($credentiasArr),
	    'store_hash' =>  $credentiasArr['store_hash']
	));
}

/**
 * @param string $credentiasArr  credentials's array
 * @return string the oauth Access (aka Auth) Token to use in API requests.
 */
function getAuthToken($credentiasArr) {
	$object = new \stdClass();
	$object->client_id =$credentiasArr['client_id'];
	$object->client_secret = $credentiasArr['client_secret'];
	$object->redirect_uri =  $credentiasArr['redirect_uri'];
	$object->code = isset($_GET['code'])?$_GET['code']:'';
	$object->context = isset($_GET['context'])?$_GET['context']:'';
	$object->scope = isset($_GET['scope'])?$_GET['scope']:'';
	
	$authToken = Bigcommerce::getAuthToken($object);
	return $authToken->access_token;
}

?>