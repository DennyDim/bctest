# BigCommerce Sample App: PHP #

This is a test page that implements the OAuth callback  for BigCommerce and uses the  Bigcommerce API Client  to pull a list of customers with specific emails on a BigCommerce store.  
Bigcommerce API Client  is PHP client for connecting to the Bigcommerce V2 REST API: https://github.com/bigcommerce/bigcommerce-api-php

### Requirements: ###

* PHP 7.0 or greater
* cUrl extension enabled
* Composer

### install/initialize the application ###

* Clone the repo.

git clone https://DennyDim@bitbucket.org/DennyDim/bctest.git
	
* Use Composer to install the dependencies.

php composer.phar install
